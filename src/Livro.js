import React, { Component } from 'react';
import { Input, Button } from './components/Helpers';
import PubSub from 'pubsub-js';

export class FormularioLivro extends Component {
  constructor() {
    super();

    this.state = {
      titulo: '',
      preco: '',
      autorId: 0
    }
  }

  enviaForm = (event) => {
    event.preventDefault();
    
    fetch('https://api-react1.herokuapp.com/livros', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        titulo: this.state.titulo, preco: this.state.preco, idAutor: this.state.autorId
      })
    })
    .then((res) => res.json())
    .then((data) => {
      PubSub.publish('nova-lista', data);
      this.setState({titulo: '', preco: '', autorId: ''});
    })
    .catch((err) => console.log(err));
  }

  render() {
    return (
      <div className="pure-form pure-form-aligned">
        <form action="https://api-react1.herokuapp.com/livros" className="pure-form pure-form-aligned" method="post" onSubmit={this.enviaForm}>
          <Input id="titulo" type="text" name="titulo" value={this.state.titulo}
            onChange={e => this.setState({ titulo: e.target.value })} label="Titulo" />

          <Input id="preco" type="text" name="preco" value={this.state.preco}
            onChange={e => this.setState({ preco: e.target.value })} label="Preço" />

          <div className="pure-control-group">
            <label htmlFor="autorId">Autor</label>
            <select value={this.state.autorId} name="autorId" id="autorId"
              onChange={e => this.setState({autorId: e.target.value})}>

              <option value="">Selecione o autor</option>
              {
                this.props.autores.map((autor, index) => (
                  <option key={index} value={autor.idAutor}>{autor.nome}</option>
                ))
              }
            </select>
          </div>

          <Button type="submit" className="pure-button pure-button-primary" label="Gravar" />
        </form>
      </div>
    )
  }
}

export class TabelaLivro extends Component {
  render() {
    return (
      <div>
        <table className="pure-table">
          <thead>
            <tr>
              <th>Titulo</th>
              <th>Preço</th>
              <th>Autor</th>
            </tr>
          </thead>
          <tbody>
            {this.props.lista.map((item, index) => (
              <tr key={index}>
                <td>{item.titulo}</td>
                <td>{item.preco}</td>
                <td>{item.Autor}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default class LivroBox extends Component {
  constructor() {
    super();

    this.state = {
      lista: [],
      autores: []
    }
  }

  componentDidMount() {

    fetch('https://api-react1.herokuapp.com/livros')
      .then(res => res.json())
      .then((data) => {
        this.setState({lista: data});
      })
      .catch((err) => console.log(err));


    fetch('https://api-react1.herokuapp.com/autores')
      .then((res) => res.json())
      .then((data) => {
        this.setState({autores: data});
      })
      .catch((err) => console.log(err));


    PubSub.subscribe('nova-lista', (topico, novaLista) => {
      
      fetch('https://api-react1.herokuapp.com/livros')
      .then(res => res.json())
      .then((data) => {
        this.setState({lista: data});
      })
      .catch((err) => console.log(err));
      
      /* this.setState(state => ({
        lista: state.lista.concat(novaLista)
      })); */
    });
  }

  render() {

    const { lista, autores } = this.state;

    return (
      <div>
        <div className="header" style={{ marginBottom: 20 }}>
          <h1>Livro</h1>
        </div>
        <div className="content" id="content">
          <FormularioLivro autores={autores}/>
          <TabelaLivro lista={lista}/>
        </div>
      </div>
    );
  }
}