import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const LINK = [
  { link: '/', value: 'Home' },
  { link: '/autor', value: 'Autor' },
  { link: '/livro', value: 'Livro' }
];

class App extends Component {
  render() {
    return (
      <div>
        <div id="menu">
          <div className="pure-menu">
            <h3 className="pure-menu-heading">Company</h3>
            <ul className="pure-menu-list">
              {this.renderLink()}
            </ul>
          </div>
        </div>

        <div id="main">
         {this.props.children}
        </div>
      </div>
    );
  }

  renderLink() {
    return LINK.map((item, index) => {
      return (
        <li key={index} className="pure-menu-item">
          <Link className="pure-menu-link" to={item.link}>{item.value}</Link>
        </li>
      );
    });
  }
}

export default App;
