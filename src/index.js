import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './css/pure-min.css';
import './css/side-menu-old-ie.css';
import './css/side-menu.css';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

import Home from './Home';
import AutorBox from './Autor';
import LivroBox from './Livro';

ReactDOM.render(
  <Router>
    <App>
      <Route exact path="/" component={Home}/>
      <Route exact path="/autor" component={AutorBox}/>
      <Route exact path="/livro" component={LivroBox}/>
    </App>
  </Router>
  , document.getElementById('root')
);

registerServiceWorker();
