import React, { Component } from 'react';
import { Input, Button } from './components/Helpers';
import PubSub from 'pubsub-js';

class FormularioAutor extends Component {
  constructor() {
    super();

    this.state = {
      nome: '',
      email: '',
      senha: ''
    }
  }

  enviaForm = (e) => {
    e.preventDefault();

    fetch('https://api-react1.herokuapp.com/autores', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ nome: this.state.nome, email: this.state.email, senha: this.state.senha })
    })
      .then(res => res.json())
      .then((results) => {
        PubSub.publish('nova-lista-autores', results);
        this.setState({
          nome: '',
          email: '',
          senha: ''
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  change = (field, event) => {
    this.setState({[field]: event.target.value})
  }

  render() {
    return (
      <div className="pure-form pure-form-aligned">
        <form action="https://api-react1.herokuapp.com/autores" className="pure-form pure-form-aligned" method="post" onSubmit={this.enviaForm}>
          <Input id="nome" type="text" name="nome" value={this.state.nome}
            onChange={e => this.change('nome',e)} label="Nome" />

          <Input id="email" type="email" name="email" value={this.state.email}
            onChange={e => this.change('email',e)} label="Email" />
            
          <Input id="senha" type="password" name="senha" value={this.state.senha}
            onChange={e => this.change('senha',e)} label="Senha" />

          <Button type="submit" className="pure-button pure-button-primary" label="Gravar" />
        </form>
      </div>
    );
  }
}

class TabelaAutores extends Component {
  render() {
    return (
      <div>
        <table className="pure-table">
          <thead>
            <tr>
              <th>Nome</th>
              <th>email</th>
            </tr>
          </thead>
          <tbody>
            {this.props.lista.map((autor, index) => (
              <tr key={index}>
                <td>{autor.nome}</td>
                <td>{autor.email}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default class AutorBox extends Component {
  constructor() {
    super();

    this.state = {
      lista: []
    }
  }

  componentDidMount() {
    fetch('https://api-react1.herokuapp.com/autores')
      .then(res => res.json())
      .then((results) => {
        this.setState({ lista: results });
      })
      .catch((error) => {
        console.log(error);
      });

    PubSub.subscribe('nova-lista-autores', (topico, novaLista) => {
      this.setState(state => ({
        lista: state.lista.concat(novaLista)
      }));
    });
  };

  render() {

    const { lista } = this.state;

    return (
      <div>
        <div className="header" style={{marginBottom:20}}>
          <h1>Autor</h1>
        </div>
        <div className="content" id="content">
          <FormularioAutor />
          <TabelaAutores lista={lista} />
        </div>
      </div>
    )
  }
}