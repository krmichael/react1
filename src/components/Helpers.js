import React from 'react';

export function Input(props) {
  return (
    <div className="pure-control-group">
      <label htmlFor={props.id}>{props.label}</label>
      <input {...props} />
    </div>
  )
}

export function Button(props) {
  return (
    <div className="pure-control-group">
      <label></label>
      <button type={props.type} className={props.className}>{props.label}</button>
    </div>
  )
}